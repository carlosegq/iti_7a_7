export class User{
    id:number;
    names:string;
    surnames:string;
    birthDate:string;
    phone:string;
    gender:string;
    age:number;
    mail:string;
    password:string;
    stars:string;
    photo:any;
    administrator:boolean;
}