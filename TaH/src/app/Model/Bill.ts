export class bill {
    id:number;
    priceRoom:string;
    nights:string;
    total:string;
    fines:string;
    idRoom:number;
    cancellation:boolean;
}