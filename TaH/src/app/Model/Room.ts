export class Room {
    id:number;
    direction:string;
    description:string;
    peopleCapacity:number;
    beds:number;
    price:number;
    stars:string;
    bathrooms:number;
    wateringCans:number;
    comments:string;
    internet:string;
    entranceTime:string;
    timeDeparture:string;
    photos:any[];
    active:boolean;
}