import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './WebPage/login/login.component';
import { UsComponent } from './WebPage/us/us.component';
import { RegisterComponent } from './WebPage/register/register.component';
import { PageComponent } from './WebPage/page/page.component';
import { RoomListComponent } from './WebApp/room-list/room-list.component';
import { ProfileComponent } from './WebApp/profile/profile.component';
import { RecordComponent } from './WebApp/record/record.component';
import { AdminComponent } from './WebApp/admin/admin.component';
import { RoomComponent } from './WebApp/room/room.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsComponent,
    RegisterComponent,
    PageComponent,
    RoomListComponent,
    ProfileComponent,
    RecordComponent,
    AdminComponent,
    RoomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
